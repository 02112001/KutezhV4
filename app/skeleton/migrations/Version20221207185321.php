<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221207185321 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE place_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE place (id INT NOT NULL, status BOOLEAN NOT NULL default true, name VARCHAR(255) NOT NULL default true, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE party ADD places TEXT NOT NULL DEFAULT false');
        $this->addSql('COMMENT ON COLUMN party.places IS \'(DC2Type:array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE place_id_seq CASCADE');
        $this->addSql('DROP TABLE place');
        $this->addSql('ALTER TABLE party DROP places');
        $this->addSql('ALTER TABLE party DROP guest_limits');
    }
}
