<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220911154944 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE guest_product (guest_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(guest_id, product_id))');
        $this->addSql('CREATE INDEX IDX_938FC0E19A4AA658 ON guest_product (guest_id)');
        $this->addSql('CREATE INDEX IDX_938FC0E14584665A ON guest_product (product_id)');
        $this->addSql('ALTER TABLE guest_product ADD CONSTRAINT FK_938FC0E19A4AA658 FOREIGN KEY (guest_id) REFERENCES guest (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE guest_product ADD CONSTRAINT FK_938FC0E14584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cheque ADD who_paid_id INT NOT NULL');
        $this->addSql('ALTER TABLE cheque ADD CONSTRAINT FK_A0BBFDE9E7D47899 FOREIGN KEY (who_paid_id) REFERENCES guest (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A0BBFDE9E7D47899 ON cheque (who_paid_id)');
        $this->addSql('ALTER TABLE payment ADD who_make_payment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD who_get_payments_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D11908BB1 FOREIGN KEY (who_make_payment_id) REFERENCES guest (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840DDE149377 FOREIGN KEY (who_get_payments_id) REFERENCES guest (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6D28840D11908BB1 ON payment (who_make_payment_id)');
        $this->addSql('CREATE INDEX IDX_6D28840DDE149377 ON payment (who_get_payments_id)');
        $this->addSql('ALTER TABLE product ADD in_cheque_id INT NOT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD5F493FE8 FOREIGN KEY (in_cheque_id) REFERENCES cheque (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D34A04AD5F493FE8 ON product (in_cheque_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE guest_product DROP CONSTRAINT FK_938FC0E19A4AA658');
        $this->addSql('ALTER TABLE guest_product DROP CONSTRAINT FK_938FC0E14584665A');
        $this->addSql('DROP TABLE guest_product');
        $this->addSql('ALTER TABLE cheque DROP CONSTRAINT FK_A0BBFDE9E7D47899');
        $this->addSql('DROP INDEX IDX_A0BBFDE9E7D47899');
        $this->addSql('ALTER TABLE cheque DROP who_paid_id');
        $this->addSql('ALTER TABLE payment DROP CONSTRAINT FK_6D28840D11908BB1');
        $this->addSql('ALTER TABLE payment DROP CONSTRAINT FK_6D28840DDE149377');
        $this->addSql('DROP INDEX IDX_6D28840D11908BB1');
        $this->addSql('DROP INDEX IDX_6D28840DDE149377');
        $this->addSql('ALTER TABLE payment DROP who_make_payment_id');
        $this->addSql('ALTER TABLE payment DROP who_get_payments_id');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD5F493FE8');
        $this->addSql('DROP INDEX IDX_D34A04AD5F493FE8');
        $this->addSql('ALTER TABLE product DROP in_cheque_id');
    }
}
