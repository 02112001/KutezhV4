<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221208183028 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE party DROP guest_limit');
        $this->addSql('ALTER TABLE party DROP places');
        $this->addSql('ALTER TABLE place ALTER status DROP DEFAULT');
        $this->addSql('ALTER TABLE place ALTER name DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE party ADD guest_limit INT NOT NULL');
        $this->addSql('ALTER TABLE party ADD places TEXT DEFAULT \'false\' NOT NULL');
        $this->addSql('COMMENT ON COLUMN party.places IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE place ALTER status SET DEFAULT true');
        $this->addSql('ALTER TABLE place ALTER name SET DEFAULT \'true\'');
    }
}
