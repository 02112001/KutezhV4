<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220912142648 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cheque ADD author_for_cheque_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cheque ADD CONSTRAINT FK_A0BBFDE9FD72D687 FOREIGN KEY (author_for_cheque_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A0BBFDE9FD72D687 ON cheque (author_for_cheque_id)');
        $this->addSql('ALTER TABLE guest ADD author_for_guest_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE guest ADD CONSTRAINT FK_ACB79A35812BC727 FOREIGN KEY (author_for_guest_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_ACB79A35812BC727 ON guest (author_for_guest_id)');
        $this->addSql('ALTER TABLE party ADD author_for_party_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE party ADD CONSTRAINT FK_89954EE03A5D7126 FOREIGN KEY (author_for_party_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_89954EE03A5D7126 ON party (author_for_party_id)');
        $this->addSql('ALTER TABLE payment ADD author_for_payment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D96D18C2D FOREIGN KEY (author_for_payment_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6D28840D96D18C2D ON payment (author_for_payment_id)');
        $this->addSql('ALTER TABLE product ADD author_for_product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADD79649CC FOREIGN KEY (author_for_product_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D34A04ADD79649CC ON product (author_for_product_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04ADD79649CC');
        $this->addSql('DROP INDEX IDX_D34A04ADD79649CC');
        $this->addSql('ALTER TABLE product DROP author_for_product_id');
        $this->addSql('ALTER TABLE guest DROP CONSTRAINT FK_ACB79A35812BC727');
        $this->addSql('DROP INDEX IDX_ACB79A35812BC727');
        $this->addSql('ALTER TABLE guest DROP author_for_guest_id');
        $this->addSql('ALTER TABLE cheque DROP CONSTRAINT FK_A0BBFDE9FD72D687');
        $this->addSql('DROP INDEX IDX_A0BBFDE9FD72D687');
        $this->addSql('ALTER TABLE cheque DROP author_for_cheque_id');
        $this->addSql('ALTER TABLE payment DROP CONSTRAINT FK_6D28840D96D18C2D');
        $this->addSql('DROP INDEX IDX_6D28840D96D18C2D');
        $this->addSql('ALTER TABLE payment DROP author_for_payment_id');
        $this->addSql('ALTER TABLE party DROP CONSTRAINT FK_89954EE03A5D7126');
        $this->addSql('DROP INDEX IDX_89954EE03A5D7126');
        $this->addSql('ALTER TABLE party DROP author_for_party_id');
    }
}
