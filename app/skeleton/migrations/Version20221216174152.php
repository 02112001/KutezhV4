<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221216174152 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX uniq_741d53cd5bd3c7fc');
        $this->addSql('CREATE INDEX IDX_741D53CD5BD3C7FC ON place (who_booking_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX IDX_741D53CD5BD3C7FC');
        $this->addSql('CREATE UNIQUE INDEX uniq_741d53cd5bd3c7fc ON place (who_booking_id)');
    }
}
