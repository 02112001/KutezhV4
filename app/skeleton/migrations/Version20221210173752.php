<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221210173752 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE party ADD partys_on_tribune_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE party ADD CONSTRAINT FK_89954EE0DA1341E0 FOREIGN KEY (partys_on_tribune_id) REFERENCES tribune (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_89954EE0DA1341E0 ON party (partys_on_tribune_id)');
        $this->addSql('ALTER TABLE place DROP CONSTRAINT fk_741d53cd213c1059');
        $this->addSql('DROP INDEX idx_741d53cd213c1059');
        $this->addSql('ALTER TABLE place RENAME COLUMN party_id TO tribune_id');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDF9E50ED6 FOREIGN KEY (tribune_id) REFERENCES tribune (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_741D53CDF9E50ED6 ON place (tribune_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE place DROP CONSTRAINT FK_741D53CDF9E50ED6');
        $this->addSql('DROP INDEX IDX_741D53CDF9E50ED6');
        $this->addSql('ALTER TABLE place RENAME COLUMN tribune_id TO party_id');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT fk_741d53cd213c1059 FOREIGN KEY (party_id) REFERENCES party (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_741d53cd213c1059 ON place (party_id)');
        $this->addSql('ALTER TABLE party DROP CONSTRAINT FK_89954EE0DA1341E0');
        $this->addSql('DROP INDEX IDX_89954EE0DA1341E0');
        $this->addSql('ALTER TABLE party DROP partys_on_tribune_id');
    }
}
