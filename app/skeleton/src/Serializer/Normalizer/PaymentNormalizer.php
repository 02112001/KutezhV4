<?php

namespace App\Serializer\Normalizer;

use App\Entity\Payment;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PaymentNormalizer implements NormalizerInterface
{
    /**
     * @param Payment $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|void|null
     */
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        $data = [
            'id' => $object->getId(),
            'dateAt' => $object->getDate(),
            'from' => $object->getWhoMakePayment() ? [
                'id' => $object->getWhoMakePayment()->getId(),
                'name' => $object->getWhoMakePayment()->getName(),
            ] : null,
            'to' => $object->getWhoGetPayments() ? $object->getToGuest()->getId() : null,
            'summ' => $object->getSumm() . 'p.',
            'advanced' => (new \DateTime())->format('Y:m:d'),
        ];
        return $data;
    }

    public function supportsNormalization(mixed $data, string $format = null)
    {
        return $data instanceof Payment;
    }

}