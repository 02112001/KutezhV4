<?php


namespace App\Serializer\Normalizer;

use App\Entity\Party;
use App\Entity\Payment;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class PartyNormalizer implements NormalizerInterface
{
    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        $data = [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'location' => $object->getLocation(),
            'Guests' => $object->getGuests() ?  : null,
            'advanced' => (new \DateTime())->format('Y:m:d'),
        ];
        return $data;
    }

    public function supportsNormalization(mixed $data, string $format = null)
    {
        return $data instanceof Payment;
    }
}