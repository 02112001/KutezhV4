<?php


namespace App\Serializer\Denormalizer;

use App\Entity\Party;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class PartyDenormalizer implements DenormalizerInterface
{
    public function denormalize(mixed $data, string $type, string $format = null, array $context = [])
    {
        $party = new Party();
        $party->setName($data['summ']);
        $data['from']['id'];
        $party->setDate(new \DateTimeImmutable());
        return $party;
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null)
    {
        return Payment::class == $type;
    }
}