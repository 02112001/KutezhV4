<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?int $price = null;

    #[ORM\Column]
    private ?float $count = null;

    #[ORM\ManyToMany(targetEntity: Guest::class, mappedBy: 'whoPays')]
    private Collection $guestsWhoPays;

    #[ORM\ManyToOne(inversedBy: 'products')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Cheque $inCheque = null;

    #[ORM\ManyToOne(inversedBy: 'products')]
    private ?User $authorForProduct = null;

    public function __construct()
    {
        $this->guestsWhoPays = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCount(): ?float
    {
        return $this->count;
    }

    public function setCount(float $count): self
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return Collection<int, Guest>
     */
    public function getGuestsWhoPays(): Collection
    {
        return $this->guestsWhoPays;
    }

    public function addGuestsWhoPay(Guest $guestsWhoPay): self
    {
        if (!$this->guestsWhoPays->contains($guestsWhoPay)) {
            $this->guestsWhoPays->add($guestsWhoPay);
            $guestsWhoPay->addWhoPay($this);
        }

        return $this;
    }

    public function removeGuestsWhoPay(Guest $guestsWhoPay): self
    {
        if ($this->guestsWhoPays->removeElement($guestsWhoPay)) {
            $guestsWhoPay->removeWhoPay($this);
        }

        return $this;
    }

    public function getInCheque(): ?Cheque
    {
        return $this->inCheque;
    }

    public function setInCheque(?Cheque $inCheque): self
    {
        $this->inCheque = $inCheque;

        return $this;
    }

    public function getAuthorForProduct(): ?User
    {
        return $this->authorForProduct;
    }

    public function setAuthorForProduct(?User $authorForProduct): self
    {
        $this->authorForProduct = $authorForProduct;

        return $this;
    }
}
