<?php

namespace App\Entity;

use App\Repository\ChequeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChequeRepository::class)]
class Cheque
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(length: 255)]
    private ?string $shop = null;

    #[ORM\ManyToOne(inversedBy: 'paiedCheques')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Guest $whoPaid = null;

    #[ORM\OneToMany(mappedBy: 'inCheque', targetEntity: Product::class)]
    private Collection $products;

    #[ORM\ManyToOne(inversedBy: 'cheques')]
    private ?User $authorForCheque = null;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getShop(): ?string
    {
        return $this->shop;
    }

    public function setShop(string $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getWhoPaid(): ?Guest
    {
        return $this->whoPaid;
    }

    public function setWhoPaid(?Guest $whoPaid): self
    {
        $this->whoPaid = $whoPaid;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->setInCheque($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getInCheque() === $this) {
                $product->setInCheque(null);
            }
        }

        return $this;
    }

    public function getAuthorForCheque(): ?User
    {
        return $this->authorForCheque;
    }

    public function setAuthorForCheque(?User $authorForCheque): self
    {
        $this->authorForCheque = $authorForCheque;

        return $this;
    }
}
