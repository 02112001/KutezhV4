<?php

namespace App\Entity;

use App\Repository\TribuneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TribuneRepository::class)]
class Tribune
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nameTribune = null;

    #[ORM\OneToMany(mappedBy: 'partysOnTribune', targetEntity: Party::class)]
    private Collection $parties;

    #[ORM\OneToMany(mappedBy: 'tribune', targetEntity: Place::class)]
    private Collection $places;

    public function __construct()
    {
        $this->parties = new ArrayCollection();
        $this->places = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameTribune(): ?string
    {
        return $this->nameTribune;
    }

    public function setNameTribune(?string $nameTribune): self
    {
        $this->nameTribune = $nameTribune;

        return $this;
    }

    /**
     * @return Collection<int, Party>
     */
    public function getParties(): Collection
    {
        return $this->parties;
    }

    public function addParty(Party $party): self
    {
        if (!$this->parties->contains($party)) {
            $this->parties->add($party);
            $party->setPartysOnTribune($this);
        }

        return $this;
    }



    public function removeParty(Party $party): self
    {
        if ($this->parties->removeElement($party)) {
            // set the owning side to null (unless already changed)
            if ($party->getPartysOnTribune() === $this) {
                $party->setPartysOnTribune(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Place>
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Place $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places->add($place);
            $place->setTribune($this);
        }

        return $this;
    }



    public function removePlace(Place $place): self
    {
        if ($this->places->removeElement($place)) {
            // set the owning side to null (unless already changed)
            if ($place->getTribune() === $this) {
                $place->setTribune(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        $name = $this->getNameTribune();

        return $name;
    }
}
