<?php

namespace App\Entity;

use App\Repository\PlaceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PlaceRepository::class)]
class Place
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?bool $status = null;



    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'places')]
    private ?Tribune $tribune = null;

    #[ORM\ManyToOne(cascade: ['persist', 'remove'])]
    private ?User $whoBooking = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /*public function getParty(): ?Party
    {
        return $this->party;
    }
    */
    public function setParty(?Party $party): self
    {
        $this->party = $party;

        return $this;
    }

    public function __toString(): string
    {
        $name = $this->getName();
        $status = 0;
        if($this->isStatus()){
            $status = "free";
        }else{
            $status = "booked";
        }

        $name = $name." ". $status;

        return $name;
    }

    public function getTribune(): ?Tribune
    {
        return $this->tribune;
    }

    public function setTribune(?Tribune $tribune): self
    {
        $this->tribune = $tribune;

        return $this;
    }

    public function getWhoBooking(): ?User
    {
        return $this->whoBooking;
    }

    public function setWhoBooking(?User $whoBooking): self
    {
        $this->whoBooking = $whoBooking;

        return $this;
    }



}
