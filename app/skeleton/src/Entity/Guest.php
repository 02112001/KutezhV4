<?php

namespace App\Entity;

use App\Repository\GuestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GuestRepository::class)]
class Guest
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $telephoneNumber = null;

    #[ORM\OneToMany(mappedBy: 'whoPaid', targetEntity: Cheque::class)]
    private Collection $paiedCheques;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'guestsWhoPays')]
    private Collection $whoPays;

    #[ORM\OneToMany(mappedBy: 'whoMakePayment', targetEntity: Payment::class)]
    private Collection $makedPayments;

    #[ORM\OneToMany(mappedBy: 'whoGetPayments', targetEntity: Payment::class)]
    private Collection $getsPayments;

    #[ORM\ManyToMany(targetEntity: Party::class, mappedBy: 'guests')]
    private Collection $parties;

    #[ORM\ManyToOne(inversedBy: 'guests')]
    private ?User $authorForGuest = null;

    public function __construct()
    {
        $this->paiedCheques = new ArrayCollection();
        $this->whoPays = new ArrayCollection();
        $this->makedPayments = new ArrayCollection();
        $this->getsPayments = new ArrayCollection();
        $this->parties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTelephoneNumber(): ?string
    {
        return $this->telephoneNumber;
    }

    public function setTelephoneNumber(?string $telephoneNumber): self
    {
        $this->telephoneNumber = $telephoneNumber;

        return $this;
    }

    /**
     * @return Collection<int, Cheque>
     */
    public function getPaiedCheques(): Collection
    {
        return $this->paiedCheques;
    }

    public function addPaiedCheque(Cheque $paiedCheque): self
    {
        if (!$this->paiedCheques->contains($paiedCheque)) {
            $this->paiedCheques->add($paiedCheque);
            $paiedCheque->setWhoPaid($this);
        }

        return $this;
    }

    public function removePaiedCheque(Cheque $paiedCheque): self
    {
        if ($this->paiedCheques->removeElement($paiedCheque)) {
            // set the owning side to null (unless already changed)
            if ($paiedCheque->getWhoPaid() === $this) {
                $paiedCheque->setWhoPaid(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getWhoPays(): Collection
    {
        return $this->whoPays;
    }

    public function addWhoPay(Product $whoPay): self
    {
        if (!$this->whoPays->contains($whoPay)) {
            $this->whoPays->add($whoPay);
        }

        return $this;
    }

    public function removeWhoPay(Product $whoPay): self
    {
        $this->whoPays->removeElement($whoPay);

        return $this;
    }

    /**
     * @return Collection<int, Payment>
     */
    public function getMakedPayments(): Collection
    {
        return $this->makedPayments;
    }

    public function addMakedPayment(Payment $makedPayment): self
    {
        if (!$this->makedPayments->contains($makedPayment)) {
            $this->makedPayments->add($makedPayment);
            $makedPayment->setWhoMakePayment($this);
        }

        return $this;
    }

    public function removeMakedPayment(Payment $makedPayment): self
    {
        if ($this->makedPayments->removeElement($makedPayment)) {
            // set the owning side to null (unless already changed)
            if ($makedPayment->getWhoMakePayment() === $this) {
                $makedPayment->setWhoMakePayment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Payment>
     */
    public function getGetsPayments(): Collection
    {
        return $this->getsPayments;
    }

    public function addGetsPayment(Payment $getsPayment): self
    {
        if (!$this->getsPayments->contains($getsPayment)) {
            $this->getsPayments->add($getsPayment);
            $getsPayment->setWhoGetPayments($this);
        }

        return $this;
    }

    public function removeGetsPayment(Payment $getsPayment): self
    {
        if ($this->getsPayments->removeElement($getsPayment)) {
            // set the owning side to null (unless already changed)
            if ($getsPayment->getWhoGetPayments() === $this) {
                $getsPayment->setWhoGetPayments(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Party>
     */
    public function getParties(): Collection
    {
        return $this->parties;
    }

    public function addParty(Party $party): self
    {
        if (!$this->parties->contains($party)) {
            $this->parties->add($party);
            $party->addGuest($this);
        }

        return $this;
    }

    public function removeParty(Party $party): self
    {
        if ($this->parties->removeElement($party)) {
            $party->removeGuest($this);
        }

        return $this;
    }

    public function getAuthorForGuest(): ?User
    {
        return $this->authorForGuest;
    }

    public function setAuthorForGuest(?User $authorForGuest): self
    {
        $this->authorForGuest = $authorForGuest;

        return $this;
    }
}
