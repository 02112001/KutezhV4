<?php

namespace App\Entity;

use App\Repository\PartyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;



#[ORM\Entity(repositoryClass: PartyRepository::class)]
class Party
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(length: 255)]
    private ?string $location = null;

    #[ORM\ManyToMany(targetEntity: Guest::class, inversedBy: 'parties')]
    private Collection $guests;

    #[ORM\ManyToOne(inversedBy: 'parties')]
    private ?User $authorForParty = null;

    #[ORM\ManyToOne(inversedBy: 'parties')]
    private ?Tribune $partysOnTribune = null;

    /*#[ORM\ManyToMany(targetEntity:Place::class ,inversedBy:'places')]
    private Collection $placesOnTribune;*/


    public function __construct()
    {
        $this->guests = new ArrayCollection();
        $this->places = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection<int, Guest>
     */
    public function getGuests(): Collection
    {
        return $this->guests;
    }

    public function addGuest(Guest $guest): self
    {
        $this->guests->add($guest);
        return $this;
    }

    public function removeGuest(Guest $guest): self
    {
        $this->guests->removeElement($guest);

        return $this;
    }

    public function getAuthorForParty(): ?User
    {
        return $this->authorForParty;
    }

    public function setAuthorForParty(?User $authorForParty): self
    {
        $this->authorForParty = $authorForParty;

        return $this;
    }

    /**
     * @return Collection<int, Place>
     */
    /**public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Place $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places->add($place);
            $place->setParty($this);
        }

        return $this;
    }

    public function removePlace(Place $place): self
    {
        if ($this->places->removeElement($place)) {
            // set the owning side to null (unless already changed)
            if ($place->getParty() === $this) {
                $place->setParty(null);
            }
        }

        return $this;
    }
    */
    public function getPartysOnTribune(): ?Tribune
    {
        return $this->partysOnTribune;
    }

    public function setPartysOnTribune(?Tribune $partysOnTribune): self
    {
        $this->partysOnTribune = $partysOnTribune;

        return $this;
    }

    /*public function getPlacesOnTribune(?Tribune $tribune): Collection
    {
        $placesOnTribune = $tribune->getPlaces();
        return $placesOnTribune;
    }
    */
}
