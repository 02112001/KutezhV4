<?php

namespace App\Entity;

use App\Repository\PaymentRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PaymentRepository::class)]
class Payment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column]
    private ?float $summ = null;

    #[ORM\ManyToOne(inversedBy: 'makedPayments')]
    private ?Guest $whoMakePayment = null;

    #[ORM\ManyToOne(inversedBy: 'getsPayments')]
    private ?Guest $whoGetPayments = null;

    #[ORM\ManyToOne(inversedBy: 'payments')]
    private ?User $authorForPayment = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getSumm(): ?float
    {
        return $this->summ;
    }

    public function setSumm(float $summ): self
    {
        $this->summ = $summ;

        return $this;
    }

    public function getWhoMakePayment(): ?Guest
    {
        return $this->whoMakePayment;
    }

    public function setWhoMakePayment(?Guest $whoMakePayment): self
    {
        $this->whoMakePayment = $whoMakePayment;

        return $this;
    }

    public function getWhoGetPayments(): ?Guest
    {
        return $this->whoGetPayments;
    }

    public function setWhoGetPayments(?Guest $whoGetPayments): self
    {
        $this->whoGetPayments = $whoGetPayments;

        return $this;
    }

    public function getAuthorForPayment(): ?User
    {
        return $this->authorForPayment;
    }

    public function setAuthorForPayment(?User $authorForPayment): self
    {
        $this->authorForPayment = $authorForPayment;

        return $this;
    }
}
