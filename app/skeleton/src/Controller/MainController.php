<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Form\PaymentType;
use App\Repository\PaymentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/main')]
class MainController extends AbstractController
{
    #[Route('/', name: 'mainPageInfo', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('pages/mainPage.html.twig');
    }

}
