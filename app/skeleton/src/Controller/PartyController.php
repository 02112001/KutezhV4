<?php

namespace App\Controller;

use App\Entity\Party;
use App\Entity\Place;
use App\Form\PartyType;
use App\Repository\PartyRepository;
use App\Repository\PlaceRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


#[Route('/party')]
class PartyController extends AbstractController
{
    #[Route('/', name: 'app_party_index', methods: ['GET'])]
    public function index(PartyRepository $partyRepository): Response
    {
        return $this->render('party/index.html.twig', [
            'parties' => $partyRepository->findAll(),
        ]);
    }

     #[Route('/switch-status/{idplace}/{idparty}', name:'app_place_switch_status', methods: ['GET'])]
    public function book(EntityManagerInterface $em, TokenStorageInterface $storage,PartyRepository $partyRepository,PlaceRepository $placeRepository, $idplace, $idparty):Response
    {
        $party = $partyRepository->find($idparty);
        $place = $placeRepository->find($idplace);
        $user= $storage->getToken()->getUser();

        $place->setWhoBooking($user);
        $place->setStatus(false);

        $em->flush();
        return $this->redirectToRoute('app_party_show', ['id'=>$party->getId()], Response::HTTP_SEE_OTHER);
    }



    #[Route('/subscribeOnParty', name: 'app_party_subscribeOnParty', methods: ['POST','GET'])]
    public function subscribeOnParty(PartyRepository $partyRepository, $party, $request): Response
    {

        $form = $this->createForm(PartyType::class, $party);
        $form->handleRequest($request);

        //if (лимит превышен) {
        //$form->get('элемент формы')->addError(new FormError("лимит превышен"));
        //}
        if ($form->isSubmitted() && $form->isValid()) {
            $partyRepository->add($party, true);

            return $this->redirectToRoute('app_party_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('party/edit.html.twig', [
            'party' => $party,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/bookPlace', name:'app_party_place_booking', methods: ['GET','POST'])]
    public function placeBooking(Place $place):Response
    {
        $place->setStatus(false);
        return $this->render('party/show.html.twig', [
            'place' => $place,
        ]);
    }

    #[Route('/new', name: 'app_party_new', methods: ['GET', 'POST'])]
    public function new(Request $request, PartyRepository $partyRepository): Response
    {
        $party = new Party();
        $form = $this->createForm(PartyType::class, $party);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $partyRepository->add($party, true);

            return $this->redirectToRoute('app_party_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('party/new.html.twig', [
            'party' => $party,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_party_show', methods: ['GET'])]
    public function show(Party $party): Response
    {
        return $this->render('party/show.html.twig', [
            'party' => $party,
        ]);
    }


    #[Route('/{id}/edit', name: 'app_party_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Party $party, PartyRepository $partyRepository): Response
    {
        $form = $this->createForm(PartyType::class, $party);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $partyRepository->add($party, true);

            return $this->redirectToRoute('app_party_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('party/edit.html.twig', [
            'party' => $party,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_party_delete', methods: ['POST'])]
    public function delete(Request $request, Party $party, PartyRepository $partyRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$party->getId(), $request->request->get('_token'))) {
            $partyRepository->remove($party, true);
        }

        return $this->redirectToRoute('app_party_index', [], Response::HTTP_SEE_OTHER);
    }
}
